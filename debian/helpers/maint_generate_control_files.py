#!/usr/bin/python3

import os
import re
import xml.etree.ElementTree as ET
import textwrap
import tempfile
from itertools import chain


SHORT_DESCRIPTION = {
        "cmake": "CMake API for linting Robot OS packages",
        "lint": "Linting integration for Robot OS packages",
        "": "Linting tools for Robot OS packages",
}
LONG_DESCRIPTION = {
        "cmake": """\
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
""",
    "lint": """\
 This package is part of Robot OS version 2 (ROS2).
 ament_lint is a set of scripts to easily integrate various linting
 tools in the Robot OS build process.
""",
    "": """\
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
""",
}


MAP_DEPS = {"uncrustify_vendor": "uncrustify"}

def parse_package_xml(path):
    xml = ET.parse(os.path.join(path, "package.xml"))
    result = {
            "name": xml.find("./name").text,
            "build_depends": set(d.text for d in chain(
                xml.findall("./depend"),
                xml.findall("./build_depend"),
                xml.findall("./buildtool_depend"),
                xml.findall("./build_export_depend"),
                xml.findall("./buildtool_export_depend"),
                xml.findall("./test_depend"),
            )),
            "depends": set(d.text for d in chain(
                xml.findall("./depend"),
                xml.findall("./build_export_depend"),
                xml.findall("./buildtool_export_depend"),
                xml.findall("./exec_depend"),
            )),
            "build_type": xml.find("./export/build_type").text,
            "description": xml.find("./description").text.strip().split(".")[0],
    }
    result["debian_name"] = result["name"].replace("_", "-")
    result["description"] = re.sub(r"\s\s+", " ", result["description"])
    result["description"] = "\n ".join(textwrap.wrap(" This package provides " + result["description"][:1].lower() + result["description"][1:] + ".", 78))
    result["has_script"] = False
    setup_py = os.path.join(path, "setup.py")
    if os.path.isfile(setup_py):
        with open(setup_py, "r") as f:
            content = f.read()
            result["has_script"] = "console_script" in content
        result["section"] = "python"
        result["debian_name"] = "python3-" + result["debian_name"]
    result["has_test"] = os.path.isdir(os.path.join(path, "test"))
    return result


debian_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
package_dir = os.path.dirname(debian_dir)

descriptions = {}
with open(os.path.join(debian_dir, "control"), "r") as f:
    content = f.read().split("\n\n")
    control_header = content.pop(0)
    for block in content:
        mo = re.search(r"Package: (\S+)", block)
        if mo:
            name = mo.group(1)
            desc = re.search(r"Description: ([^\n]*(?:\n [^\n]+)+)", block).group(1)
            descriptions[name] = desc

packages = {}
for path in os.listdir(package_dir):
    path = os.path.join(package_dir, path)
    if os.path.isfile(os.path.join(path, "package.xml")):
        package = parse_package_xml(path)
        packages[package["name"]] = package

with tempfile.TemporaryDirectory(prefix=debian_dir) as tmp_dir:
    dh_python_helpers = []
    with open(os.path.join(tmp_dir, "build-rules.mk"), "w") as f:
        f.write("export CMAKE_PREFIX_PATH=$(CURDIR)/debian/tmp/usr\n")
        f.write("export PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/$(shell py3versions -d)/dist-packages\n")
        f.write("export PYBUILD_DESTDIR=debian/tmp\n")
        f.write("\n")
        f.write("build_all: " + " ".join("build_" + name for name in sorted(packages.keys())) + "\n\n.PHONY: build_all\n\n")
        for name in sorted(packages.keys()):
            package = packages[name]
            install_files = [
                "usr/share/%s" % name,
                "usr/share/ament_index/resource_index/*/%s" % name,
            ]
            f.write("build_" + name + ": " + " ".join("build_" + name for name in sorted(package["build_depends"]) if name in packages.keys()) + "\n")
            f.write(
                "\t###\n"
                "\t### Building package %s\n"
                "\t###\n" % name
            )
            if "cmake" in package["build_type"]:
                f.write(
                    "\tdh_auto_configure --buildsystem=cmake --package=%(debian_name)s --sourcedir=%(name)s --builddir=.build/%(name)s --tmpdir=debian/tmp\n"
                    "\tdh_auto_build --buildsystem=cmake --package=%(debian_name)s --sourcedir=%(name)s --builddir=.build/%(name)s --tmpdir=debian/tmp\n"
                    "ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))\n"
                    "\tdh_auto_test --buildsystem=cmake --package=%(debian_name)s --sourcedir=%(name)s --builddir=.build/%(name)s --tmpdir=debian/tmp\n"
                    "endif\n"
                    "\tdh_auto_install --buildsystem=cmake --package=%(debian_name)s --sourcedir=%(name)s --builddir=.build/%(name)s --tmpdir=debian/tmp\n"
                    % {
                        "name": name,
                        "debian_name": package["debian_name"],
                    }
                )
            elif "python" in package["build_type"]:
                f.write(
                    "\trm -rf .pybuild\n"
                    "\tdh_auto_configure --buildsystem=pybuild --package=%(debian_name)s --sourcedir=%(name)s\n"
                    "\tdh_auto_build --buildsystem=pybuild --package=%(debian_name)s --sourcedir=%(name)s\n"
                    "\tdh_auto_install --buildsystem=pybuild --package=%(debian_name)s --sourcedir=%(name)s\n"
                    "ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))\n"
                    "\t%(skip_test)sdh_auto_test --buildsystem=pybuild --package=%(debian_name)s --sourcedir=%(name)s\n"
                    "endif\n"
                    % {
                        "name": name,
                        "debian_name": package["debian_name"],
                        "skip_test": "" if package["has_test"] else "#",
                    }
                )
                if package["has_script"]:
                    f.write(
                        "\tmkdir -p .build/manpages\n"
                        "\tCOLUMNS=999 debian/tmp/usr/bin/%(name)s --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \\\n"
                        "\thelp2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n \"linting tool for Robot OS packages\" cat | \\\n"
                        "\tsed -e 's#\<cat\>#%(name)s#g' -e 's#\<CAT\>#%(NAME)s#g' > .build/manpages/%(name)s.1\n"
                        % {
                            "name": name,
                            "NAME": name.upper(),
                        }
                    )
                    install_files.append("usr/bin/%s" % name)
                    with open(os.path.join(tmp_dir, package["debian_name"] + ".manpages"), "w") as g:
                        g.write(".build/manpages/%s.1" % name)
                install_files.append("usr/lib/python3*/dist-packages/%s" % name)
                install_files.append("usr/lib/python3*/dist-packages/%s-*.egg-info" % name)
            else:
                f.write("\tfalse\n")
            with open(os.path.join(tmp_dir, package["debian_name"] + ".install"), "w") as g:
                g.write("\n".join(install_files))
            f.write("\n.PHONY: build_" + name + "\n\n")

    with open(os.path.join(tmp_dir, "control"), "w") as f:
        f.write(control_header)
        f.write("\n")
        for name in sorted(packages.keys()):
            package = packages[name]
            if name.startswith("ament_cmake"):
                subtype = "cmake"
                addon = name[12:]
            elif name.startswith("ament_lint"):
                subtype = "lint"
                addon = name[11:]
            else:
                subtype = ""
                addon = name[6:]
            depends = set(["${misc:Depends}"])
            if "python" in package["build_type"]:
                depends.add("${python3:Depends}")
                if not package["debian_name"].startswith("python3-"):
                    depends.add("python3-setuptools")
            for d in package["depends"]:
                if d in packages.keys():
                    depends.add(packages[d]["debian_name"])
                else:
                    d_name = MAP_DEPS.get(d, d.replace("_", "-"))
                    if d_name is not None:
                        depends.add(d_name)
            f.write(
                "\n"
                "Package: %(debian_name)s\n"
                "%(section)s"
                "Architecture: all\n"
                "Multi-Arch: foreign\n"
                "Depends: %(depend)s\n"
                "Description: %(short_desc)s\n"
                "%(long_desc)s"
                " .\n"
                "%(module_desc)s\n"
                % {
                    "debian_name": package["debian_name"],
                    "section": "Section: %s\n" % package["section"] if "section" in package else "",
                    "depend": ", ".join(sorted(depends)),
                    "short_desc": SHORT_DESCRIPTION[subtype] + (" (%s)" % addon if addon else ""),
                    "long_desc": LONG_DESCRIPTION[subtype],
                    "module_desc": package["description"],
                }
            )

    for name in os.listdir(tmp_dir):
        os.rename(os.path.join(tmp_dir, name), os.path.join(debian_dir, name))

