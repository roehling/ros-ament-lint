export CMAKE_PREFIX_PATH=$(CURDIR)/debian/tmp/usr
export PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/$(shell py3versions -d)/dist-packages
export PYBUILD_DESTDIR=debian/tmp

build_all: build_ament_clang_format build_ament_clang_tidy build_ament_cmake_clang_format build_ament_cmake_clang_tidy build_ament_cmake_copyright build_ament_cmake_cppcheck build_ament_cmake_cpplint build_ament_cmake_flake8 build_ament_cmake_lint_cmake build_ament_cmake_mypy build_ament_cmake_pclint build_ament_cmake_pep257 build_ament_cmake_pep8 build_ament_cmake_pyflakes build_ament_cmake_uncrustify build_ament_cmake_xmllint build_ament_copyright build_ament_cppcheck build_ament_cpplint build_ament_flake8 build_ament_lint build_ament_lint_auto build_ament_lint_cmake build_ament_lint_common build_ament_mypy build_ament_pclint build_ament_pep257 build_ament_pep8 build_ament_pyflakes build_ament_uncrustify build_ament_xmllint

.PHONY: build_all

build_ament_clang_format: build_ament_copyright build_ament_flake8 build_ament_pep257
	###
	### Building package ament_clang_format
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-clang-format --sourcedir=ament_clang_format
	dh_auto_build --buildsystem=pybuild --package=python3-ament-clang-format --sourcedir=ament_clang_format
	dh_auto_install --buildsystem=pybuild --package=python3-ament-clang-format --sourcedir=ament_clang_format
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-clang-format --sourcedir=ament_clang_format
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_clang_format --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_clang_format#g' -e 's#\<CAT\>#AMENT_CLANG_FORMAT#g' > .build/manpages/ament_clang_format.1

.PHONY: build_ament_clang_format

build_ament_clang_tidy: build_ament_copyright build_ament_flake8 build_ament_pep257
	###
	### Building package ament_clang_tidy
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-clang-tidy --sourcedir=ament_clang_tidy
	dh_auto_build --buildsystem=pybuild --package=python3-ament-clang-tidy --sourcedir=ament_clang_tidy
	dh_auto_install --buildsystem=pybuild --package=python3-ament-clang-tidy --sourcedir=ament_clang_tidy
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-clang-tidy --sourcedir=ament_clang_tidy
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_clang_tidy --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_clang_tidy#g' -e 's#\<CAT\>#AMENT_CLANG_TIDY#g' > .build/manpages/ament_clang_tidy.1

.PHONY: build_ament_clang_tidy

build_ament_cmake_clang_format: build_ament_clang_format build_ament_cmake_copyright build_ament_cmake_lint_cmake
	###
	### Building package ament_cmake_clang_format
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-clang-format --sourcedir=ament_cmake_clang_format --builddir=.build/ament_cmake_clang_format --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-clang-format --sourcedir=ament_cmake_clang_format --builddir=.build/ament_cmake_clang_format --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-clang-format --sourcedir=ament_cmake_clang_format --builddir=.build/ament_cmake_clang_format --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-clang-format --sourcedir=ament_cmake_clang_format --builddir=.build/ament_cmake_clang_format --tmpdir=debian/tmp

.PHONY: build_ament_cmake_clang_format

build_ament_cmake_clang_tidy: build_ament_clang_tidy build_ament_cmake_copyright build_ament_cmake_lint_cmake
	###
	### Building package ament_cmake_clang_tidy
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-clang-tidy --sourcedir=ament_cmake_clang_tidy --builddir=.build/ament_cmake_clang_tidy --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-clang-tidy --sourcedir=ament_cmake_clang_tidy --builddir=.build/ament_cmake_clang_tidy --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-clang-tidy --sourcedir=ament_cmake_clang_tidy --builddir=.build/ament_cmake_clang_tidy --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-clang-tidy --sourcedir=ament_cmake_clang_tidy --builddir=.build/ament_cmake_clang_tidy --tmpdir=debian/tmp

.PHONY: build_ament_cmake_clang_tidy

build_ament_cmake_copyright: build_ament_cmake_lint_cmake build_ament_copyright
	###
	### Building package ament_cmake_copyright
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-copyright --sourcedir=ament_cmake_copyright --builddir=.build/ament_cmake_copyright --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-copyright --sourcedir=ament_cmake_copyright --builddir=.build/ament_cmake_copyright --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-copyright --sourcedir=ament_cmake_copyright --builddir=.build/ament_cmake_copyright --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-copyright --sourcedir=ament_cmake_copyright --builddir=.build/ament_cmake_copyright --tmpdir=debian/tmp

.PHONY: build_ament_cmake_copyright

build_ament_cmake_cppcheck: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_cppcheck
	###
	### Building package ament_cmake_cppcheck
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-cppcheck --sourcedir=ament_cmake_cppcheck --builddir=.build/ament_cmake_cppcheck --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-cppcheck --sourcedir=ament_cmake_cppcheck --builddir=.build/ament_cmake_cppcheck --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-cppcheck --sourcedir=ament_cmake_cppcheck --builddir=.build/ament_cmake_cppcheck --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-cppcheck --sourcedir=ament_cmake_cppcheck --builddir=.build/ament_cmake_cppcheck --tmpdir=debian/tmp

.PHONY: build_ament_cmake_cppcheck

build_ament_cmake_cpplint: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_cpplint
	###
	### Building package ament_cmake_cpplint
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-cpplint --sourcedir=ament_cmake_cpplint --builddir=.build/ament_cmake_cpplint --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-cpplint --sourcedir=ament_cmake_cpplint --builddir=.build/ament_cmake_cpplint --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-cpplint --sourcedir=ament_cmake_cpplint --builddir=.build/ament_cmake_cpplint --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-cpplint --sourcedir=ament_cmake_cpplint --builddir=.build/ament_cmake_cpplint --tmpdir=debian/tmp

.PHONY: build_ament_cmake_cpplint

build_ament_cmake_flake8: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_flake8
	###
	### Building package ament_cmake_flake8
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-flake8 --sourcedir=ament_cmake_flake8 --builddir=.build/ament_cmake_flake8 --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-flake8 --sourcedir=ament_cmake_flake8 --builddir=.build/ament_cmake_flake8 --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-flake8 --sourcedir=ament_cmake_flake8 --builddir=.build/ament_cmake_flake8 --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-flake8 --sourcedir=ament_cmake_flake8 --builddir=.build/ament_cmake_flake8 --tmpdir=debian/tmp

.PHONY: build_ament_cmake_flake8

build_ament_cmake_lint_cmake: build_ament_lint_cmake
	###
	### Building package ament_cmake_lint_cmake
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-lint-cmake --sourcedir=ament_cmake_lint_cmake --builddir=.build/ament_cmake_lint_cmake --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-lint-cmake --sourcedir=ament_cmake_lint_cmake --builddir=.build/ament_cmake_lint_cmake --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-lint-cmake --sourcedir=ament_cmake_lint_cmake --builddir=.build/ament_cmake_lint_cmake --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-lint-cmake --sourcedir=ament_cmake_lint_cmake --builddir=.build/ament_cmake_lint_cmake --tmpdir=debian/tmp

.PHONY: build_ament_cmake_lint_cmake

build_ament_cmake_mypy: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_mypy
	###
	### Building package ament_cmake_mypy
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-mypy --sourcedir=ament_cmake_mypy --builddir=.build/ament_cmake_mypy --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-mypy --sourcedir=ament_cmake_mypy --builddir=.build/ament_cmake_mypy --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-mypy --sourcedir=ament_cmake_mypy --builddir=.build/ament_cmake_mypy --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-mypy --sourcedir=ament_cmake_mypy --builddir=.build/ament_cmake_mypy --tmpdir=debian/tmp

.PHONY: build_ament_cmake_mypy

build_ament_cmake_pclint: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_pclint
	###
	### Building package ament_cmake_pclint
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-pclint --sourcedir=ament_cmake_pclint --builddir=.build/ament_cmake_pclint --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-pclint --sourcedir=ament_cmake_pclint --builddir=.build/ament_cmake_pclint --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-pclint --sourcedir=ament_cmake_pclint --builddir=.build/ament_cmake_pclint --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-pclint --sourcedir=ament_cmake_pclint --builddir=.build/ament_cmake_pclint --tmpdir=debian/tmp

.PHONY: build_ament_cmake_pclint

build_ament_cmake_pep257: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_pep257
	###
	### Building package ament_cmake_pep257
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-pep257 --sourcedir=ament_cmake_pep257 --builddir=.build/ament_cmake_pep257 --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-pep257 --sourcedir=ament_cmake_pep257 --builddir=.build/ament_cmake_pep257 --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-pep257 --sourcedir=ament_cmake_pep257 --builddir=.build/ament_cmake_pep257 --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-pep257 --sourcedir=ament_cmake_pep257 --builddir=.build/ament_cmake_pep257 --tmpdir=debian/tmp

.PHONY: build_ament_cmake_pep257

build_ament_cmake_pep8: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_pep8
	###
	### Building package ament_cmake_pep8
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-pep8 --sourcedir=ament_cmake_pep8 --builddir=.build/ament_cmake_pep8 --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-pep8 --sourcedir=ament_cmake_pep8 --builddir=.build/ament_cmake_pep8 --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-pep8 --sourcedir=ament_cmake_pep8 --builddir=.build/ament_cmake_pep8 --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-pep8 --sourcedir=ament_cmake_pep8 --builddir=.build/ament_cmake_pep8 --tmpdir=debian/tmp

.PHONY: build_ament_cmake_pep8

build_ament_cmake_pyflakes: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_pyflakes
	###
	### Building package ament_cmake_pyflakes
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-pyflakes --sourcedir=ament_cmake_pyflakes --builddir=.build/ament_cmake_pyflakes --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-pyflakes --sourcedir=ament_cmake_pyflakes --builddir=.build/ament_cmake_pyflakes --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-pyflakes --sourcedir=ament_cmake_pyflakes --builddir=.build/ament_cmake_pyflakes --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-pyflakes --sourcedir=ament_cmake_pyflakes --builddir=.build/ament_cmake_pyflakes --tmpdir=debian/tmp

.PHONY: build_ament_cmake_pyflakes

build_ament_cmake_uncrustify: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_uncrustify
	###
	### Building package ament_cmake_uncrustify
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-uncrustify --sourcedir=ament_cmake_uncrustify --builddir=.build/ament_cmake_uncrustify --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-uncrustify --sourcedir=ament_cmake_uncrustify --builddir=.build/ament_cmake_uncrustify --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-uncrustify --sourcedir=ament_cmake_uncrustify --builddir=.build/ament_cmake_uncrustify --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-uncrustify --sourcedir=ament_cmake_uncrustify --builddir=.build/ament_cmake_uncrustify --tmpdir=debian/tmp

.PHONY: build_ament_cmake_uncrustify

build_ament_cmake_xmllint: build_ament_cmake_copyright build_ament_cmake_lint_cmake build_ament_xmllint
	###
	### Building package ament_cmake_xmllint
	###
	dh_auto_configure --buildsystem=cmake --package=ament-cmake-xmllint --sourcedir=ament_cmake_xmllint --builddir=.build/ament_cmake_xmllint --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-cmake-xmllint --sourcedir=ament_cmake_xmllint --builddir=.build/ament_cmake_xmllint --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-cmake-xmllint --sourcedir=ament_cmake_xmllint --builddir=.build/ament_cmake_xmllint --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-cmake-xmllint --sourcedir=ament_cmake_xmllint --builddir=.build/ament_cmake_xmllint --tmpdir=debian/tmp

.PHONY: build_ament_cmake_xmllint

build_ament_copyright: build_ament_flake8 build_ament_pep257
	###
	### Building package ament_copyright
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-copyright --sourcedir=ament_copyright
	dh_auto_build --buildsystem=pybuild --package=python3-ament-copyright --sourcedir=ament_copyright
	dh_auto_install --buildsystem=pybuild --package=python3-ament-copyright --sourcedir=ament_copyright
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-copyright --sourcedir=ament_copyright
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_copyright --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_copyright#g' -e 's#\<CAT\>#AMENT_COPYRIGHT#g' > .build/manpages/ament_copyright.1

.PHONY: build_ament_copyright

build_ament_cppcheck: 
	###
	### Building package ament_cppcheck
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-cppcheck --sourcedir=ament_cppcheck
	dh_auto_build --buildsystem=pybuild --package=python3-ament-cppcheck --sourcedir=ament_cppcheck
	dh_auto_install --buildsystem=pybuild --package=python3-ament-cppcheck --sourcedir=ament_cppcheck
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	#dh_auto_test --buildsystem=pybuild --package=python3-ament-cppcheck --sourcedir=ament_cppcheck
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_cppcheck --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_cppcheck#g' -e 's#\<CAT\>#AMENT_CPPCHECK#g' > .build/manpages/ament_cppcheck.1

.PHONY: build_ament_cppcheck

build_ament_cpplint: build_ament_copyright build_ament_flake8 build_ament_pep257
	###
	### Building package ament_cpplint
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-cpplint --sourcedir=ament_cpplint
	dh_auto_build --buildsystem=pybuild --package=python3-ament-cpplint --sourcedir=ament_cpplint
	dh_auto_install --buildsystem=pybuild --package=python3-ament-cpplint --sourcedir=ament_cpplint
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-cpplint --sourcedir=ament_cpplint
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_cpplint --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_cpplint#g' -e 's#\<CAT\>#AMENT_CPPLINT#g' > .build/manpages/ament_cpplint.1

.PHONY: build_ament_cpplint

build_ament_flake8: 
	###
	### Building package ament_flake8
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-flake8 --sourcedir=ament_flake8
	dh_auto_build --buildsystem=pybuild --package=python3-ament-flake8 --sourcedir=ament_flake8
	dh_auto_install --buildsystem=pybuild --package=python3-ament-flake8 --sourcedir=ament_flake8
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-flake8 --sourcedir=ament_flake8
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_flake8 --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_flake8#g' -e 's#\<CAT\>#AMENT_FLAKE8#g' > .build/manpages/ament_flake8.1

.PHONY: build_ament_flake8

build_ament_lint: 
	###
	### Building package ament_lint
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-lint --sourcedir=ament_lint
	dh_auto_build --buildsystem=pybuild --package=python3-ament-lint --sourcedir=ament_lint
	dh_auto_install --buildsystem=pybuild --package=python3-ament-lint --sourcedir=ament_lint
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	#dh_auto_test --buildsystem=pybuild --package=python3-ament-lint --sourcedir=ament_lint
endif

.PHONY: build_ament_lint

build_ament_lint_auto: 
	###
	### Building package ament_lint_auto
	###
	dh_auto_configure --buildsystem=cmake --package=ament-lint-auto --sourcedir=ament_lint_auto --builddir=.build/ament_lint_auto --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-lint-auto --sourcedir=ament_lint_auto --builddir=.build/ament_lint_auto --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-lint-auto --sourcedir=ament_lint_auto --builddir=.build/ament_lint_auto --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-lint-auto --sourcedir=ament_lint_auto --builddir=.build/ament_lint_auto --tmpdir=debian/tmp

.PHONY: build_ament_lint_auto

build_ament_lint_cmake: build_ament_copyright build_ament_flake8 build_ament_pep257
	###
	### Building package ament_lint_cmake
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-lint-cmake --sourcedir=ament_lint_cmake
	dh_auto_build --buildsystem=pybuild --package=python3-ament-lint-cmake --sourcedir=ament_lint_cmake
	dh_auto_install --buildsystem=pybuild --package=python3-ament-lint-cmake --sourcedir=ament_lint_cmake
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-lint-cmake --sourcedir=ament_lint_cmake
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_lint_cmake --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_lint_cmake#g' -e 's#\<CAT\>#AMENT_LINT_CMAKE#g' > .build/manpages/ament_lint_cmake.1

.PHONY: build_ament_lint_cmake

build_ament_lint_common: 
	###
	### Building package ament_lint_common
	###
	dh_auto_configure --buildsystem=cmake --package=ament-lint-common --sourcedir=ament_lint_common --builddir=.build/ament_lint_common --tmpdir=debian/tmp
	dh_auto_build --buildsystem=cmake --package=ament-lint-common --sourcedir=ament_lint_common --builddir=.build/ament_lint_common --tmpdir=debian/tmp
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=cmake --package=ament-lint-common --sourcedir=ament_lint_common --builddir=.build/ament_lint_common --tmpdir=debian/tmp
endif
	dh_auto_install --buildsystem=cmake --package=ament-lint-common --sourcedir=ament_lint_common --builddir=.build/ament_lint_common --tmpdir=debian/tmp

.PHONY: build_ament_lint_common

build_ament_mypy: build_ament_flake8
	###
	### Building package ament_mypy
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-mypy --sourcedir=ament_mypy
	dh_auto_build --buildsystem=pybuild --package=python3-ament-mypy --sourcedir=ament_mypy
	dh_auto_install --buildsystem=pybuild --package=python3-ament-mypy --sourcedir=ament_mypy
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-mypy --sourcedir=ament_mypy
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_mypy --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_mypy#g' -e 's#\<CAT\>#AMENT_MYPY#g' > .build/manpages/ament_mypy.1

.PHONY: build_ament_mypy

build_ament_pclint: build_ament_copyright build_ament_flake8 build_ament_pep257
	###
	### Building package ament_pclint
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-pclint --sourcedir=ament_pclint
	dh_auto_build --buildsystem=pybuild --package=python3-ament-pclint --sourcedir=ament_pclint
	dh_auto_install --buildsystem=pybuild --package=python3-ament-pclint --sourcedir=ament_pclint
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-pclint --sourcedir=ament_pclint
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_pclint --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_pclint#g' -e 's#\<CAT\>#AMENT_PCLINT#g' > .build/manpages/ament_pclint.1

.PHONY: build_ament_pclint

build_ament_pep257: build_ament_flake8
	###
	### Building package ament_pep257
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-pep257 --sourcedir=ament_pep257
	dh_auto_build --buildsystem=pybuild --package=python3-ament-pep257 --sourcedir=ament_pep257
	dh_auto_install --buildsystem=pybuild --package=python3-ament-pep257 --sourcedir=ament_pep257
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-pep257 --sourcedir=ament_pep257
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_pep257 --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_pep257#g' -e 's#\<CAT\>#AMENT_PEP257#g' > .build/manpages/ament_pep257.1

.PHONY: build_ament_pep257

build_ament_pep8: 
	###
	### Building package ament_pep8
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-pep8 --sourcedir=ament_pep8
	dh_auto_build --buildsystem=pybuild --package=python3-ament-pep8 --sourcedir=ament_pep8
	dh_auto_install --buildsystem=pybuild --package=python3-ament-pep8 --sourcedir=ament_pep8
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	#dh_auto_test --buildsystem=pybuild --package=python3-ament-pep8 --sourcedir=ament_pep8
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_pep8 --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_pep8#g' -e 's#\<CAT\>#AMENT_PEP8#g' > .build/manpages/ament_pep8.1

.PHONY: build_ament_pep8

build_ament_pyflakes: build_ament_pep8
	###
	### Building package ament_pyflakes
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-pyflakes --sourcedir=ament_pyflakes
	dh_auto_build --buildsystem=pybuild --package=python3-ament-pyflakes --sourcedir=ament_pyflakes
	dh_auto_install --buildsystem=pybuild --package=python3-ament-pyflakes --sourcedir=ament_pyflakes
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-pyflakes --sourcedir=ament_pyflakes
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_pyflakes --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_pyflakes#g' -e 's#\<CAT\>#AMENT_PYFLAKES#g' > .build/manpages/ament_pyflakes.1

.PHONY: build_ament_pyflakes

build_ament_uncrustify: 
	###
	### Building package ament_uncrustify
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-uncrustify --sourcedir=ament_uncrustify
	dh_auto_build --buildsystem=pybuild --package=python3-ament-uncrustify --sourcedir=ament_uncrustify
	dh_auto_install --buildsystem=pybuild --package=python3-ament-uncrustify --sourcedir=ament_uncrustify
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	#dh_auto_test --buildsystem=pybuild --package=python3-ament-uncrustify --sourcedir=ament_uncrustify
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_uncrustify --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_uncrustify#g' -e 's#\<CAT\>#AMENT_UNCRUSTIFY#g' > .build/manpages/ament_uncrustify.1

.PHONY: build_ament_uncrustify

build_ament_xmllint: build_ament_copyright build_ament_flake8 build_ament_pep257
	###
	### Building package ament_xmllint
	###
	rm -rf .pybuild
	dh_auto_configure --buildsystem=pybuild --package=python3-ament-xmllint --sourcedir=ament_xmllint
	dh_auto_build --buildsystem=pybuild --package=python3-ament-xmllint --sourcedir=ament_xmllint
	dh_auto_install --buildsystem=pybuild --package=python3-ament-xmllint --sourcedir=ament_xmllint
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test --buildsystem=pybuild --package=python3-ament-xmllint --sourcedir=ament_xmllint
endif
	mkdir -p .build/manpages
	COLUMNS=999 debian/tmp/usr/bin/ament_xmllint --help | sed -e 's#$(CURDIR)/debian/tmp##g' | \
	help2man -N --version-string=$(DEB_VERSION_UPSTREAM) -h- -n "linting tool for Robot OS packages" cat | \
	sed -e 's#\<cat\>#ament_xmllint#g' -e 's#\<CAT\>#AMENT_XMLLINT#g' > .build/manpages/ament_xmllint.1

.PHONY: build_ament_xmllint

