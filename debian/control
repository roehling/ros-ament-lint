Source: ros-ament-lint
Section: devel
Priority: optional
Maintainer: Timo Röhling <timo.roehling@fkie.fraunhofer.de>
Build-Depends: debhelper-compat (= 12), cmake, python3-all, dh-python,
               ament-cmake, ament-cmake-core, python3-setuptools,
               help2man, python3-yaml,
               ament-cmake-test <!nocheck>, python3-pytest <!nocheck>,
               python3-flake8 <!nocheck>, python3-pydocstyle <!nocheck>,
               python3-mypy <!nocheck>, python3-pytest-mock <!nocheck>,
               python3-pep8 <!nocheck>
Standards-Version: 4.5.0
Homepage: https://github.com/ament/ament_cmake
Rules-Requires-Root: no

Package: python3-ament-clang-format
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, clang-format, python3-yaml
Description: Linting tools for Robot OS packages (clang_format)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check code against style conventions
 using clang-format and generate xUnit test result files.

Package: python3-ament-clang-tidy
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, clang-tidy, python3-yaml
Description: Linting tools for Robot OS packages (clang_tidy)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check code against style conventions
 using clang-tidy and generate xUnit test result files.

Package: ament-cmake-clang-format
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-clang-format
Description: CMake API for linting Robot OS packages (clang_format)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_clang_format to lint C / C++
 code using clang format.

Package: ament-cmake-clang-tidy
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-clang-tidy
Description: CMake API for linting Robot OS packages (clang_tidy)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_clang_tidy to lint C / C++ code
 using clang tidy.

Package: ament-cmake-copyright
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-copyright
Description: CMake API for linting Robot OS packages (copyright)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_copyright to check every source
 file contains copyright reference.

Package: ament-cmake-cppcheck
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core, ament-cmake-test, python3-ament-cppcheck
Description: CMake API for linting Robot OS packages (cppcheck)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_cppcheck to perform static code
 analysis on C/C++ code using Cppcheck.

Package: ament-cmake-cpplint
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-cpplint
Description: CMake API for linting Robot OS packages (cpplint)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_cpplint to lint C / C++ code
 using cpplint.

Package: ament-cmake-flake8
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-flake8
Description: CMake API for linting Robot OS packages (flake8)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_flake8 to check code syntax and
 style conventions with flake8.

Package: ament-cmake-lint-cmake
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-lint-cmake
Description: CMake API for linting Robot OS packages (lint_cmake)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_lint_cmake to lint CMake code
 using cmakelint.

Package: ament-cmake-mypy
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-mypy
Description: CMake API for linting Robot OS packages (mypy)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_mypy to perform static type
 analysis on python code with mypy.

Package: ament-cmake-pclint
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-pclint
Description: CMake API for linting Robot OS packages (pclint)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_pclint to perform static code
 analysis on C/C++ code using PCLint.

Package: ament-cmake-pep257
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-pep257
Description: CMake API for linting Robot OS packages (pep257)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_pep257 to check code against
 the style conventions in PEP 257.

Package: ament-cmake-pep8
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-pep8
Description: CMake API for linting Robot OS packages (pep8)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_pep8 to check code against the
 style conventions in PEP 8.

Package: ament-cmake-pyflakes
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-pyflakes
Description: CMake API for linting Robot OS packages (pyflakes)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_pyflakes to check code using
 pyflakes.

Package: ament-cmake-uncrustify
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-uncrustify
Description: CMake API for linting Robot OS packages (uncrustify)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_uncrustify to check code
 against styleconventions using uncrustify.

Package: ament-cmake-xmllint
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, python3-ament-xmllint
Description: CMake API for linting Robot OS packages (xmllint)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the CMake API for ament_xmllint to check XML file using
 xmmlint.

Package: python3-ament-copyright
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, python3-ament-lint
Description: Linting tools for Robot OS packages (copyright)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check source files for copyright and
 license information.

Package: python3-ament-cppcheck
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, cppcheck
Description: Linting tools for Robot OS packages (cppcheck)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to perform static code analysis on C/C++
 code using Cppcheck and generate xUnit test result files.

Package: python3-ament-cpplint
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}
Description: Linting tools for Robot OS packages (cpplint)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check code against the Google style
 conventions using cpplint and generate xUnit test result files.

Package: python3-ament-flake8
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, python3-ament-lint, python3-flake8
Description: Linting tools for Robot OS packages (flake8)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check code for style and syntax
 conventions with flake8.

Package: python3-ament-lint
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}
Description: Linting integration for Robot OS packages
 This package is part of Robot OS version 2 (ROS2).
 ament_lint is a set of scripts to easily integrate various linting
 tools in the Robot OS build process.
 .
 This package provides providing common API for ament linter packages.

Package: ament-lint-auto
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core, ament-cmake-test
Description: Linting integration for Robot OS packages (auto)
 This package is part of Robot OS version 2 (ROS2).
 ament_lint is a set of scripts to easily integrate various linting
 tools in the Robot OS build process.
 .
 This package provides the auto-magic functions for ease to use of the ament
 linters in CMake.

Package: python3-ament-lint-cmake
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}
Description: Linting integration for Robot OS packages (cmake)
 This package is part of Robot OS version 2 (ROS2).
 ament_lint is a set of scripts to easily integrate various linting
 tools in the Robot OS build process.
 .
 This package provides the ability to lint CMake code using cmakelint and
 generate xUnit test result files.

Package: ament-lint-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-copyright, ament-cmake-core, ament-cmake-cppcheck, ament-cmake-cpplint, ament-cmake-flake8, ament-cmake-lint-cmake, ament-cmake-pep257, ament-cmake-uncrustify, ament-cmake-xmllint
Description: Linting integration for Robot OS packages (common)
 This package is part of Robot OS version 2 (ROS2).
 ament_lint is a set of scripts to easily integrate various linting
 tools in the Robot OS build process.
 .
 This package provides the list of commonly used linters in the ament
 buildsytem in CMake.

Package: python3-ament-mypy
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, python3-mypy
Description: Linting tools for Robot OS packages (mypy)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides support for mypy static type checking in ament.

Package: python3-ament-pclint
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}
Description: Linting tools for Robot OS packages (pclint)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to perform static code analysis on C/C++
 code using pclint and generate xUnit test result files.

Package: python3-ament-pep257
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, pydocstyle, python3-ament-lint
Description: Linting tools for Robot OS packages (pep257)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check code against the style conventions
 in PEP 8 and generate xUnit test result files.

Package: python3-ament-pep8
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, python3-pep8
Description: Linting tools for Robot OS packages (pep8)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check code against the style conventions
 in PEP 8 and generate xUnit test result files.

Package: python3-ament-pyflakes
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, pyflakes3
Description: Linting tools for Robot OS packages (pyflakes)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check code using pyflakes and generate
 xUnit test result files.

Package: python3-ament-uncrustify
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, uncrustify
Description: Linting tools for Robot OS packages (uncrustify)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check code against style conventions
 using uncrustify and generate xUnit test result files.

Package: python3-ament-xmllint
Section: python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, libxml2-utils, python3-ament-lint
Description: Linting tools for Robot OS packages (xmllint)
 This package is part of Robot OS version 2 (ROS2).
 The ament buildsystem integrates various linting tools into the
 build process.
 .
 This package provides the ability to check XML files like the package
 manifest using xmllint and generate xUnit test result files.
